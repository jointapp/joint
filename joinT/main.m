//
//  main.m
//  joinT
//
//  Created by Yehoshua Matamoros valverde on 4/28/15.
//  Copyright (c) 2015 Yehoshua Matamoros valverde. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
